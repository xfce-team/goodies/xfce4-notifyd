/*
 * This file is generated by gdbus-codegen, do not modify it.
 *
 * The license of this code is the same as for the D-Bus interface description
 * it was derived from. Note that it links to GLib, so must comply with the
 * LGPL linking clauses.
 */

#ifndef __XFCE_NOTIFY_GBUS_H__
#define __XFCE_NOTIFY_GBUS_H__

#include <gio/gio.h>

G_BEGIN_DECLS


/* ------------------------------------------------------------------------ */
/* Declarations for org.xfce.Notifyd */

#define XFCE_TYPE_NOTIFY_GBUS (xfce_notify_gbus_get_type ())
#define XFCE_NOTIFY_GBUS(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), XFCE_TYPE_NOTIFY_GBUS, XfceNotifyGBus))
#define XFCE_IS_NOTIFY_GBUS(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), XFCE_TYPE_NOTIFY_GBUS))
#define XFCE_NOTIFY_GBUS_GET_IFACE(o) (G_TYPE_INSTANCE_GET_INTERFACE ((o), XFCE_TYPE_NOTIFY_GBUS, XfceNotifyGBusIface))

struct _XfceNotifyGBus;
typedef struct _XfceNotifyGBus XfceNotifyGBus;
typedef struct _XfceNotifyGBusIface XfceNotifyGBusIface;

struct _XfceNotifyGBusIface
{
  GTypeInterface parent_iface;

  gboolean (*handle_quit) (
    XfceNotifyGBus *object,
    GDBusMethodInvocation *invocation);

};

GType xfce_notify_gbus_get_type (void) G_GNUC_CONST;

GDBusInterfaceInfo *xfce_notify_gbus_interface_info (void);
guint xfce_notify_gbus_override_properties (GObjectClass *klass, guint property_id_begin);


/* D-Bus method call completion functions: */
void xfce_notify_gbus_complete_quit (
    XfceNotifyGBus *object,
    GDBusMethodInvocation *invocation);



/* D-Bus method calls: */
void xfce_notify_gbus_call_quit (
    XfceNotifyGBus *proxy,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);

gboolean xfce_notify_gbus_call_quit_finish (
    XfceNotifyGBus *proxy,
    GAsyncResult *res,
    GError **error);

gboolean xfce_notify_gbus_call_quit_sync (
    XfceNotifyGBus *proxy,
    GCancellable *cancellable,
    GError **error);



/* ---- */

#define XFCE_TYPE_NOTIFY_GBUS_PROXY (xfce_notify_gbus_proxy_get_type ())
#define XFCE_NOTIFY_GBUS_PROXY(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), XFCE_TYPE_NOTIFY_GBUS_PROXY, XfceNotifyGBusProxy))
#define XFCE_NOTIFY_GBUS_PROXY_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), XFCE_TYPE_NOTIFY_GBUS_PROXY, XfceNotifyGBusProxyClass))
#define XFCE_NOTIFY_GBUS_PROXY_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), XFCE_TYPE_NOTIFY_GBUS_PROXY, XfceNotifyGBusProxyClass))
#define XFCE_IS_NOTIFY_GBUS_PROXY(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), XFCE_TYPE_NOTIFY_GBUS_PROXY))
#define XFCE_IS_NOTIFY_GBUS_PROXY_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), XFCE_TYPE_NOTIFY_GBUS_PROXY))

typedef struct _XfceNotifyGBusProxy XfceNotifyGBusProxy;
typedef struct _XfceNotifyGBusProxyClass XfceNotifyGBusProxyClass;
typedef struct _XfceNotifyGBusProxyPrivate XfceNotifyGBusProxyPrivate;

struct _XfceNotifyGBusProxy
{
  /*< private >*/
  GDBusProxy parent_instance;
  XfceNotifyGBusProxyPrivate *priv;
};

struct _XfceNotifyGBusProxyClass
{
  GDBusProxyClass parent_class;
};

GType xfce_notify_gbus_proxy_get_type (void) G_GNUC_CONST;

#if GLIB_CHECK_VERSION(2, 44, 0)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XfceNotifyGBusProxy, g_object_unref)
#endif

void xfce_notify_gbus_proxy_new (
    GDBusConnection     *connection,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GAsyncReadyCallback  callback,
    gpointer             user_data);
XfceNotifyGBus *xfce_notify_gbus_proxy_new_finish (
    GAsyncResult        *res,
    GError             **error);
XfceNotifyGBus *xfce_notify_gbus_proxy_new_sync (
    GDBusConnection     *connection,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GError             **error);

void xfce_notify_gbus_proxy_new_for_bus (
    GBusType             bus_type,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GAsyncReadyCallback  callback,
    gpointer             user_data);
XfceNotifyGBus *xfce_notify_gbus_proxy_new_for_bus_finish (
    GAsyncResult        *res,
    GError             **error);
XfceNotifyGBus *xfce_notify_gbus_proxy_new_for_bus_sync (
    GBusType             bus_type,
    GDBusProxyFlags      flags,
    const gchar         *name,
    const gchar         *object_path,
    GCancellable        *cancellable,
    GError             **error);


/* ---- */

#define XFCE_TYPE_NOTIFY_GBUS_SKELETON (xfce_notify_gbus_skeleton_get_type ())
#define XFCE_NOTIFY_GBUS_SKELETON(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), XFCE_TYPE_NOTIFY_GBUS_SKELETON, XfceNotifyGBusSkeleton))
#define XFCE_NOTIFY_GBUS_SKELETON_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), XFCE_TYPE_NOTIFY_GBUS_SKELETON, XfceNotifyGBusSkeletonClass))
#define XFCE_NOTIFY_GBUS_SKELETON_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), XFCE_TYPE_NOTIFY_GBUS_SKELETON, XfceNotifyGBusSkeletonClass))
#define XFCE_IS_NOTIFY_GBUS_SKELETON(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), XFCE_TYPE_NOTIFY_GBUS_SKELETON))
#define XFCE_IS_NOTIFY_GBUS_SKELETON_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), XFCE_TYPE_NOTIFY_GBUS_SKELETON))

typedef struct _XfceNotifyGBusSkeleton XfceNotifyGBusSkeleton;
typedef struct _XfceNotifyGBusSkeletonClass XfceNotifyGBusSkeletonClass;
typedef struct _XfceNotifyGBusSkeletonPrivate XfceNotifyGBusSkeletonPrivate;

struct _XfceNotifyGBusSkeleton
{
  /*< private >*/
  GDBusInterfaceSkeleton parent_instance;
  XfceNotifyGBusSkeletonPrivate *priv;
};

struct _XfceNotifyGBusSkeletonClass
{
  GDBusInterfaceSkeletonClass parent_class;
};

GType xfce_notify_gbus_skeleton_get_type (void) G_GNUC_CONST;

#if GLIB_CHECK_VERSION(2, 44, 0)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XfceNotifyGBusSkeleton, g_object_unref)
#endif

XfceNotifyGBus *xfce_notify_gbus_skeleton_new (void);


G_END_DECLS

#endif /* __XFCE_NOTIFY_GBUS_H__ */
